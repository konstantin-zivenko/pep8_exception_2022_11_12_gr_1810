class MyClass:
    def fist_method(self):
        return None

    def second_method(self):
        return True


class MySecondClass:
    pass


def top_level_function():
    pass


def function(arg_one: int, arg_two: str, arg_three: dict, arg_four: list = None) -> str:
    return f"our result - {arg_one}, \
    {arg_two}, {arg_three} and {arg_four}"


total = fist_variable + second_variable - third_variable


x = 3
if x > 5:
    print("x is lager than 5")


def function(arg_one: int, arg_two: str, arg_three: dict, arg_four: list = None) -> str:
    return f"our result - {arg_one}, \
    {arg_two}, {arg_three} and {arg_four}"


x = 3
if x > 5 and x < 10:
    # Comment
    print(
        "x is lager than 5"
        "vvvvvvvvvvv\
         bbbbbbbbbbbbbbb"
    )


x = 3
if x > 5 and x < 10:
    print("x is lager than 5")

list_of_numbers = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
]

for i in range(0, 10):
    # Loop over i ten times and print out the value of i, followed by a
    # new line character
    print(i, "\n")


def quadratic(a, b, c, x):
    # Calculate the solution to a quadratic equation using the quadratic
    # formula.
    #
    # There are always two solutions to a quadratic equation, x_1 and x_2.
    x_1 = (-b + (b**2 - 4 * a * c) ** (1 / 2)) / (2 * a)
    x_2 = (-b - (b**2 - 4 * a * c) ** (1 / 2)) / (2 * a)
    return x_1, x_2


x = 5  # This is an inline comment

name = "John Smith"  # Student name
student_name = "John Smith"


def quadratic(a, b, c, x):
    """Solve quadratic equation via the quadratic formula.

    A quadratic equation has the following form:
    ax**2 + bx + c = 0

    There always two solutions to a quadratic equation: x_1 & x_2.
    """
    x_1 = (-b + (b**2 - 4 * a * c) ** (1 / 2)) / (2 * a)
    x_2 = (-b - (b**2 - 4 * a * c) ** (1 / 2)) / (2 * a)

    return x_1, x_2


# = += -= *=
# ==, !=, >, <, =<, =>, is, is not, in, not int
# and, not, or, &, |

a = "fff"
if a is not None:
    pass


for i in range(0, 3):
    for j in range(0, 3):
        if i == 2:
            print(i, j)
